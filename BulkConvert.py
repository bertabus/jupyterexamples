#!/bin/env python
import glob, os, sys
from subprocess import call
from shutil import copyfile
from nbformat import v3, v4

def convertFileToNB(filename, htmlList, kernelType):
    print("\n**********converting ", filename, "********************")
    if os.path.exists(filename):
        base = os.path.basename(filename)
        base_noext = os.path.splitext(base)[0]
        path = os.path.dirname(filename)
        print(base)
        print(base_noext)
        print(path)
        with open(filename) as fpin:
            text = fpin.read()
        text += """
# <markdowncell>
# [download ipynb file](./"""+base_noext+""".ipynb)

# <markdowncell>
        
# If you can read this, reads_py() is no longer broken! 
        """
        # print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        # print(text)
        # print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
        nbook = v3.reads_py(text)
        nbook = v4.upgrade(nbook)  # Upgrade v3 to v4
        jsonform = v4.writes(nbook) + "\n"
        with open(path+"/"+base_noext+".ipynb", "w") as fpout:
            fpout.write(jsonform)
        if kernelType == "py3":
        	call(["jupyter", "nbconvert", "--inplace", "--execute", path+"/"+base_noext+".ipynb"])
        	call(["jupyter", "nbconvert", "--to", "html", path+"/"+base_noext+".ipynb"])
        if kernelType == "py2":
        	call(["jupyter", "nbconvert", "--inplace", "--execute", "--ExecutePreprocessor.kernel_name=python2", path+"/"+base_noext+".ipynb"])
        	call(["jupyter", "nbconvert", "--to", "html", "--ExecutePreprocessor.kernel_name=python2", path+"/"+base_noext+".ipynb"])
        if kernelType == "r":
        	call(["jupyter", "nbconvert", "--inplace", "--execute", "--ExecutePreprocessor.kernel_name=ir", path+"/"+base_noext+".ipynb"])
        	call(["jupyter", "nbconvert", "--to", "html", "--ExecutePreprocessor.kernel_name=ir", path+"/"+base_noext+".ipynb"])
        htmlList.append(path+"/"+base_noext+".html")
  

htmlList = []
for filename in glob.iglob("Examples/**/*.ipy", recursive=True):
    convertFileToNB(filename,htmlList,"py3")
for filename in glob.iglob("Examples/**/*.ipy2", recursive=True):
    convertFileToNB(filename,htmlList,"py2")
for filename in glob.iglob("Examples/**/*.rpy", recursive=True):
    convertFileToNB(filename,htmlList,"r")
print("htmlList =")
print(htmlList)
for item in htmlList:
    os.makedirs(os.path.dirname("public/"+item), exist_ok=True)
    copyfile(item,"public/"+item)
with open("public/"+"index.html", "w") as fpout:
    htmlhead="""
    <!DOCTYPE html>
    <html>
    <body>
    <h2>HTML Links</h2>
    """
    fpout.write(htmlhead)
    for item in htmlList:
        fpout.write("<p><a href=\""+item+"\">"+item+"</a></p>")
        fpout.write("\n")
    htmltail="""
    </body>
    </html>
    """
    fpout.write(htmltail)

# copy these files into the public upload folder for viewing
includeFiles = [".png",".ipynb",".v",".log",".net",".erc",".tex"]
for type in includeFiles:
    for filename in glob.iglob("Examples/**/*"+type, recursive=True):
        # folder should already exist
        copyfile(filename,"public/"+filename)

print("done")


